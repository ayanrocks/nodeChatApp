const expect = require("expect");
const { Users } = require("./users");

describe("Users", () => {
  var users;
  beforeEach(() => {
    users = new Users();
    users.users = [
      {
        id: "1",
        name: "Mike",
        room: "Node Course"
      },
      {
        id: "2",
        name: "Jake",
        room: "Graphql Course"
      },
      {
        id: "3",
        name: "Rami",
        room: "Node Course"
      }
    ];
  });

  it("should add new user", () => {
    var users = new Users();
    var user = {
      id: "123",
      name: "Ayan",
      room: "The Office Fans"
    };

    var resUser = users.addUser(user.id, user.name, user.room);

    expect(users.users).toEqual([user]);
  });

  it("Should remove a user", () => {
    var userList = users.removeUser("2");

    expect(userList).toEqual({ id: "2", name: "Jake", room: "Graphql Course" });
  });

  it("Should not remove a user", () => {
    var userList = users.removeUser("5");

    expect(userList).toEqual(undefined);
  });

  it("Should find a user", () => {
    var userList = users.getUser("2");

    expect(userList).toEqual([
      { id: "2", name: "Jake", room: "Graphql Course" }
    ]);
  });
  it("Should not find a user", () => {
    var userList = users.getUser("5");

    expect(userList).toEqual([]);
  });

  it("should return names for node course", () => {
    var userList = users.getUserList("Node Course");

    expect(userList).toEqual(["Mike", "Rami"]);
  });

  it("should return names for graphql course", () => {
    var userList = users.getUserList("Graphql Course");

    expect(userList).toEqual(["Jake"]);
  });
});
