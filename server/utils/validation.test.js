const expect = require("expect");
const { isRealString } = require("./validation.js");

describe("isRealString", () => {
  it("Should reject non string values", () => {
    var res = isRealString(90);

    expect(res).toBe(false);
  });

  it("should accept strings", () => {
    var res = isRealString("Hi");

    expect(res).toBe(true);
  });
});
