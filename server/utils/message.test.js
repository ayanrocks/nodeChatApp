var expect = require('expect')
var {generateMessage, generateLocationMessage} = require("./message")
describe('generateMessage', () => {
	it("Should Generate correct message", () => {
		var from = "Jen";
		var text = 'Some Message';
		var message = generateMessage(from,text);

		expect(typeof message.createdAt).toBe("number");
		expect(message).toMatchObject({
			from,
			text
		})
	})
})

describe("generateLocationMessage", () => {
	it('should generate current location object', () => {
		var from = "Deb";
		var lat = 15;
		var long = 19;
		var url = 'https://google.com/maps?q=15,19';
		var message = generateLocationMessage(from, lat, long)
	expect(typeof message.createdAt).toBe("number");
	expect(message).toMatchObject({from, url})
	})


})