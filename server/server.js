const express = require("express");
const app = express();
const http = require("http");
const path = require("path");
const socketIO = require("socket.io");
const publicPath = path.join(__dirname + "../public");
const { Users } = require("./utils/users");
const { generateMessage, generateLocationMessage } = require("./utils/message");

const { isRealString } = require("./utils/validation");

var server = http.createServer(app);
var io = socketIO(server);
var users = new Users();

app.use(express.static("public"));

io.on("connection", socket => {
  console.log("New User Connected");

  socket.on("join", (params, cb) => {
    if (!isRealString(params.name) || !isRealString(params.room)) {
      return cb("Name and room is required");
    }

    socket.join(params.room);
    users.removeUser(socket.id);
    // io.emit -> io.to("The Office Fans")
    // socket.broadcast.emit -> socket.broadcast.to("The Office Fans")
    // socket.emit
    io.to(params.room).emit("updateUserList", users.getUserList(params.room));
    users.addUser(socket.id, params.name, params.room);

    socket.emit(
      "newMessage",
      generateMessage("Admin", "Welcome to the Chat App")
    );

    socket.broadcast
      .to(params.room)
      .emit(
        "newMessage",
        generateMessage("Admin", `${params.name} has joined`)
      );

    cb();
  });

  socket.on("createMessage", (message, callback) => {
    var user = users.getUser(socket.id);
    if (user && isRealString(message.text)) {
      io.to(users.room).emit(
        "newMessage",
        generateMessage(user.name, message.text)
      );
    }
    callback();
  });

  socket.on("createLocationMessage", coords => {
    var user = users.getUser(socket.id);
    if (user) {
      io.to(user.room).emit(
        "newLocationMessage",
        generateLocationMessage(user.name, coords.latitude, coords.longitude)
      );
    }
  });

  socket.on("disconnect", () => {
    var user = users.removeUser(socket.id);

    if (user) {
      io.to(user.room).emit("updateUserList", users.getUserList(user.room));
      io.to(user.room).emit(
        "newMessage",
        generateMessage("Admin", `${user.name} has left the room`)
      );
    }
  });
});

app.get("/", (req, res) => {
  res.render("index");
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, (req, res) => {
  console.log("Server Started at 3000");
});
