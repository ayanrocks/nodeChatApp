//SERVER


io.on("connection", socket => {
    console.log("New User Connected")

    socket.emit("newEmail", {
        from:"mike@example.com",
        subject: "Heilo"
    })

    socket.on("createEmail", (newEmail) => {
        console.log(newEmail)
    })
});


//CLIENT

var socket = io();


socket.on("connect", function() {
    console.log("Connected TO SERVER")

    socket.emit("createEmail", {
        to:"Jen@ecpamp.efe",
        text: "Hie"
    })
});

socket.on("disconnect", function()  {
    console.log("Disconnected")
})

socket.on("newEmail", function(email){
    console.log("new Email", email)
})
